import math
import numpy
import argparse
from scipy.constants import c, pi

# laser spectrum: TODO
NLUFT = 1.00028 #precise for laser wavelength? TODO

# Konstanten
# c     Vakuumlichgeschwindigkeit
# pi    Pi


#TODO Maxima aus Daten auslesen 
maxima = [5,7,9,13]	# Wellenlängen der Maxima/nm

def frequenciesFromDiffenrences(maxima):
	frequencies = []	# Freqenzdaten der Form [ Wellenlänge/nm, Spektrogrammfrequenz/(1/nm)
	for i in xrange(len(maxima)-1):
		frequencies.append([(maxima[i]+maxima[i+1])/2.0, 
