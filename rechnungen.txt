Variablen:
	d	Dicke der Probe
	c	Lichtgeschwindigkeit
	nl	Brechungsindex von Luft

######################################
Bedingungen:
	- Strahlteiler 50:50
	- keine Wegkorrektur mit Probe


	ohne Probe:
		E_out = E_in/sqrt(2) * (1 + exp(i* phi_0(omega)))
		I_out(omega) = I_in * (1 + cos(phi_0(omega)))
					 = I_in * (1 + cos(2d*omega/c))

		=> Messung von I_out(omega) liefert I_in(omega)
	
	mit Probe:
		E_out = E_in/sqrt(2) * (1 + exp(i*2d*n*omega/c))
		I_out = I_in * (1 + cos(phi(omega)))
			  = I_in * (1 + cos(omega*2d*(np-nl)/c))
		=> I_out/I_in - 1 = cos(...)
