from numpy import *
import Gnuplot

LASERFUNC = 'gauss'
LAMBMIN = 200.0   # linke Grenze des Messbereiches
LAMBMAX = 800.0   # rechte Grenze des Messbereiches
LAMBINT = 5.0     # Abstand zwischen zwei benachbarten Messpunkten

LAMBMAXPOW = 780.0		# Leistungsspitze des Lasers [nm]
POWER = 1.0			# Leistung des Lasers [rel]
LAMBSTDDEV	= 50.0	# Standardabweichung des Leistungsspektrums [nm]


def spectGauss(lamb):
    "Gaussfoermiges Leistungsspektrum"
    return exp(-((lamb-LAMBMAXPOW)/LAMBSTDDEV)**2)

laserfunc_map = {'gauss' : spectGauss}

def loadData():
    "gibt numpyarray mit Daten des Leistungsspektrums zurueck" 
    lambs = arange(LAMBMIN, LAMBMAX, LAMBINT)
    data = column_stack((lambs, vectorize(laserfunc_map[LASERFUNC])(lambs)))
    return data

def plotData(data):
    "plottet Daten aus data"
    g = Gnuplot.Gnuplot(debug=1)
    g("set term wxt")
    g.plot(data)
    raw_input("weiter mit ENTER\n")


plotData(loadData())
