%% gruppenindex für serie '4-SF6'

code = '4-SF6';
number = 11;

measurements = [11, 12, 13, 14, 15, 16, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
mids = [430, 421, 394, 369, 354, 331, 256, 280, 300, 342, 366, 390, 425, 443, 460, 495, 522];
sigmas = [160, 180, 200, 200, 200, 200, 100, 90, 80, 70, 65, 65, 65, 65, 65, 60, 55];
maxs = [3e4, 4e4, 4.1e4, 5.5e4, 5.5e4, 6.4e4, 1.6e5, 1.5e5, 1.337e5, 1.1e5, 1e5, 8.9e4, 7e4, 6e4, 5e4, 4.4e4, 3.7e4];
vorzeichen = [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

fourierAll = mapFft(code);
params = getSeriesParams(code);
dicke = cell2mat(params(6))/1000;
ergebnis = [];
for count = 1:length(measurements)
    number = measurements(count);
    delta = getDelta(code, number)/1000;
    fourier = fourierAll(number,:);

    if length(measurements)~=length(mids) && count==length(measurements)
        subplot(2,1,1)
        imagesc(log(abs(fourierAll)))
        colormap(jet)

        subplot(2,1,2)
        plot(abs(fourier))
        title(sprintf('%s, n°%i', code, number))
        mid = [];
    else
        mid = mids(count);
        sigma = sigmas(count);
        max = maxs(count);
        
        fourier = fourier./max;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ergebnis = [ergebnis; ng];
        if count==length(measurements)
            subplot(2,1,1)
            plot(NaN) 
            hold on
            plot(abs(fourier))
            ylim([-0.2, 1.2])
            xlim([mid-1.5*sigma, mid+1.5*sigma])
            plot(cutter)
            plot(abs(cut))
            hold off

            subplot(2,1,2)
            plot(NaN)
            hold on
            plot(nus(2:end), ng)
            title(sprintf('gruppenindex %s, n°%i', code, number))
            if (code(3) == 'B')
                plot(tbk7nus, tbk7)
            elseif (code(3) == 'S')
                plot(tsf6nus, tsf6)
            end
            %ylim([1.5, 1.7])
            xlim([330, 400])
            hold off
        end
    end
    
end
%% gruppenindex für serie '4-BK7'

code = '4-BK7';
number = 11;

measurements = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,35,36,37,38,39,40,41,42,43,44,45,46];
mids = [499,474,448,421,407,382,360,333,309,273,250,229,203,170,150,184,207,228,253,280,311,336,360,382,413,434,461];
sigmas = [80,80,80,80,80,80,80,80,80,80,80,80,80,60,50,50,50,50,50,50,60,65,70,70,80,60,70];
maxs = [4e4, 5e4,6.4e4,8e4,9.4e4,1.1e5,1.36e5,1.55e5,1.9e5,2.4e5,3e5,3.7e5,4.7e5,6.5e5,8e5,4e5,4.5e5,3.7e5,3e5,2.3e5,1.8e5,1.6e5,1.3e5,1.2e5,1e5,7e4,5.5e4];
vorzeichen = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1];

fourierAll = mapFft(code);
params = getSeriesParams(code);
dicke = cell2mat(params(6))/1000;
ergebnis = [];
for count = 1:length(measurements)
    number = measurements(count);
    delta = getDelta(code, number)/1000;
    fourier = fourierAll(number,:);

    if length(measurements)~=length(mids) && count==length(measurements)
        subplot(2,1,1)
        imagesc(log(abs(fourierAll)))
        colormap(jet)

        subplot(2,1,2)
        plot(abs(fourier))
        title(sprintf('%s, n°%i', code, number))
        ylim([0, 2e5])
        xlim([300, 550])
        
    else
        mid = mids(count);
        sigma = sigmas(count);
        max = maxs(count);
        
        fourier = fourier./max;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ergebnis = [ergebnis; ng];
        if count==length(measurements)
            subplot(2,1,1)
            plot(NaN) 
            hold on
            plot(abs(fourier))
            ylim([-0.2, 1.2])
            xlim([mid-1.5*sigma, mid+1.5*sigma])
            plot(cutter)
            plot(abs(cut))
            hold off

            subplot(2,1,2)
            plot(NaN)
            hold on
            plot(nus(2:end), ng)
            title(sprintf('gruppenindex %s, n%i', code, number))
            if (code(3) == 'B')
                plot(tbk7nus, tbk7)
            elseif (code(3) == 'S')
                plot(tsf6nus, tsf6)
            end
            ylim([1.45, 1.65])
            %xlim([330, 400])
            hold off
        end
    end
    
end
%% darstellung ergebnis
grid = meshgrid(1:size(ergebnis, 1), nus(2:end));
hold all
plot(nus(2:end), ergebnis.')
plot(tbk7nus, tbk7, 'b')
hold off

%% 
mittel = mean(ergebnis);
abweichung = std(ergebnis);
plot(NaN)
hold all
plot(nus(2:end), mittel, 'LineWidth',1);
plot(nus(2:end), mittel+abweichung);
plot(nus(2:end), mittel-abweichung);
plot(tbk7nus, tbk7, 'b')
plot(tsf6nus, tsf6, 'b')
hold off


