% Bilder für das Protokoll erstellen
%% lokale parameter
    addpath(genpath('/net/cip/home/paulphilipp.meyer/PPrakt/matlab'))
    load('data2/constants')
    load('data2/klotz')
    ill = 710; % interessantes linkes Lambda (darstellung)
    irl = 890; % interessantes rechtes Lambda (darstellung)
    iln = lamb2nu(irl);
    irn = lamb2nu(ill);
    set(groot, 'defaulttextinterpreter','latex')
    set(groot, 'defaultAxesTickLabelInterpreter','latex')
    set(groot, 'defaultLegendInterpreter','latex');
    set(groot, 'defaultAxesFontName', 'Times')
    set(groot, 'defaultUicontrolFontName', 'Times')
    set(groot, 'defaultUitableFontName', 'Times')
    set(groot, 'defaultAxesFontName', 'Times')
    set(groot, 'defaultTextFontName', 'Times')
    set(groot, 'defaultUipanelFontName', 'Times')
    set(groot,'defaultAxesColorOrder',[0 0 0; 0 0 0]);

    %% Interspektrogramm 4-0
    code = '4-0';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = mapRaw(code)./1e4;
    kappa = getkappa(code, 1:numDat);
    data = data/max(data(:));
    
    figure('Units','centimeters', 'Position',[0 0 13.25 7],'PaperPositionMode','auto')
    imagesc(lambs, kappa, data);
    xlim([680, 860])
    colorbar
    xlabel('$\lambda_0$ [nm]')
    ylabel('$\kappa$ [mm]')
    ylabel(colorbar,'$S(\lambda_0)$ [rel. Einheiten]', 'interpreter', 'latex')
    %colormap(parula)
    colormap(hot)
    %colormap(jet)
    print -depsc2 images/interspektro4-0.eps
%% Interspektrogramm 4-BK7
    code = '4-BK7';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = mapRaw(code)./1e4;
    kappa = getkappa(code, 1:numDat);
    data = data/max(data(:));
    imagesc(lambs, kappa, data);
    xlim([680, 860])
    colorbar
    xlabel('$\lambda_0$ [nm]')
    ylabel('$\kappa$ [mm]')
    ylabel(colorbar,'$S(\lambda_0)$ [rel. Einheiten]', 'interpreter', 'latex')
    %colormap(parula)
    colormap(hot)
    %colormap(jet)
    print(['images/interspektro', code, '.eps'], '-depsc2')

%% Interspektrogramm 4-SF6
    code = '4-SF6';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = mapRaw(code)./1e4;
    kappa = getkappa(code, 1:numDat);
    data = data/max(data(:));
    imagesc(lambs, kappa, data);
    xlim([680, 860])
    colorbar
    xlabel('$\lambda_0$ [nm]')
    ylabel('$\kappa$ [mm]')
    ylabel(colorbar,'$S(\lambda_0)$ [rel. Einheiten]', 'interpreter', 'latex')
    %colormap(parula)
    colormap(hot)
    %colormap(jet)
    print(['images/interspektro', code, '.eps'], '-depsc2')


    
%% PRO Interspektrogramme zusammen
    figure('Units','centimeters', 'Position',[0 0 13.25 20],'PaperPositionMode','auto');
    pos = get(gcf,'Position');
    %set(gcf,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
    ax = gca;
    fig = gcf;
    outerpos = ax.OuterPosition;
    ti = ax.TightInset; 
    left = outerpos(1) + ti(1);
    bottom = outerpos(2) + ti(2);
    ax_width = outerpos(3) - ti(1) - ti(3);
    ax_height = outerpos(4) - ti(2) - ti(4);
    ax.Position = [left bottom ax_width ax_height];
    fig = gcf;
    fig.PaperPositionMode = 'auto';
    fig_pos = fig.PaperPosition;
    fig.PaperSize = [fig_pos(3) fig_pos(4)];

    code = '4-0';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = mapRaw(code)./1e4;
    kappa = getkappa(code, 1:numDat);
    data = data/max(data(:));
    
    subplot(3,1,1)
    yyaxis right
    ylim([0 numDat])
    set(gca, 'Ydir', 'reverse')
    ylabel('Nummer der Messung')
    yyaxis left
    set(fig ,'defaultAxesColorOrder',[0 0 0; 0 0 0]);
    imagesc(lambs, kappa, data);
    xlim([680, 860])
    colorbar
    xlabel('$\lambda_0$ [nm]')
    ylabel('$\kappa$ [mm]')
    ylabel(colorbar,'$S(\lambda_0)$ [rel. Einheiten]', 'interpreter', 'latex')
    colormap(hot)
    title('Messung ohne Probe')
    
    subplot(3,1,2)
    code = '4-BK7';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = mapRaw(code)./1e4;
    kappa = getkappa(code, 1:numDat);
    data = data/max(data(:));
    
    
    yyaxis right
    ylim([0 numDat])
    set(gca, 'Ydir', 'reverse')
    ylabel('Nummer der Messung')
    yyaxis left
    imagesc(lambs, kappa, data);
    xlim([680, 860])
    colorbar
    xlabel('$\lambda_0$ [nm]')
    ylabel('$\kappa$ [mm]')
    ylabel(colorbar,'$S(\lambda_0)$ [rel. Einheiten]', 'interpreter', 'latex')
    colormap(hot)
    title('Messung mit BK7')

    subplot(3,1,3)
    code = '4-SF6';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = mapRaw(code)./1e4;
    kappa = getkappa(code, 1:numDat);
    data = data/max(data(:));
    
    yyaxis right
    ylim([0 numDat])
    set(gca, 'Ydir', 'reverse')
    ylabel('Nummer der Messung')
    yyaxis left
    imagesc(lambs, kappa, data);
    xlim([680, 860])
    colorbar
    xlabel('$\lambda_0$ [nm]')
    ylabel('$\kappa$ [mm]')
    ylabel(colorbar,'$S(\lambda_0)$ [rel. Einheiten]', 'interpreter', 'latex')
    colormap(hot)
    title('Messung mit SF6')
    
    print('images/interspektrogemeinsam.pdf', '-dpdf')
    
    
%% Fouriertransformation 4-0
    code = '4-0';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = abs(mapFourier(code));
    data = log(data)/max(log(data(:)));
    
    imagesc(linspace(-1,1,2048), getrelverschiebung(code, 1:numDat), data);
    colorbar
    colormap(jet)
    xlabel('$\tau$ [rel. Einheiten]')
    ylabel('$l$ [mm]')
    ylabel(colorbar,'$\log|S(\tau)|$ [$S$ in dimensionslosen Einheiten]', 'interpreter', 'latex')
    
    print(['images/fourier', code, '.eps'], '-depsc2')
    
%% Fouriertransformation 4-BK7
    code = '4-BK7';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = abs(mapFourier(code));
    data = log(data)/max(log(data(:)));
    
    imagesc(linspace(-1,1,2048), getrelverschiebung(code, 1:numDat), data);
    colorbar
    colormap(jet)
    xlabel('$\tau$ [rel. Einheiten]')
    ylabel('$l$ [mm]')
    ylabel(colorbar,'$\log|S(\tau)|$ [$S$ in dimensionslosen Einheiten]', 'interpreter', 'latex')
    
    print(['images/fourier', code, '.eps'], '-depsc2')
    
%% Fouriertransformation 4-SF6
    code = '4-SF6';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = abs(mapFourier(code));
    data = log(data)/max(log(data(:)));
    
    imagesc(linspace(-1,1,2048), getrelverschiebung(code, 1:numDat), data);
    colorbar
    colormap(jet)
    xlabel('$\tau$ [rel. Einheiten]')
    ylabel('$l$ [mm]')
    ylabel(colorbar,'$\log|S(\tau)|$ [$S$ in dimensionslosen Einheiten]', 'interpreter', 'latex')
    
    print(['images/fourier', code, '.eps'], '-depsc2')

%% PRO Fouriertransformation zusammen
    figure('Units','centimeters', 'Position',[0 0 13.25 20],'PaperPositionMode','auto');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
    figuresize(13.25, 20, 'cm')

    code = '4-0';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = abs(mapFourier(code));
    data = log(data)/max(log(data(:)));
    
    subplot(3,1,1)
    yyaxis right
    ylim([0 numDat])
    set(gca, 'Ydir', 'reverse')
    ylabel('Nummer der Messung')
    yyaxis left
    imagesc(linspace(-1,1,2048), getrelverschiebung(code, 1:numDat), data);
    colorbar
    colormap(cmap2)
    xlabel('$\tau$ [rel. Einheiten]')
    ylabel('$l$ [mm]')
    ylabel(colorbar,'$\log_{\max|S|}|S(\tau)|$', 'interpreter', 'latex')
    
    title('Messung ohne Probe')

    code = '4-BK7';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = abs(mapFourier(code));
    data = log(data)/log(max(data(:)));
    
    subplot(3,1,2)
    yyaxis right
    ylim([0 numDat])
    set(gca, 'Ydir', 'reverse')
    ylabel('Nummer der Messung')
    yyaxis left
    imagesc(linspace(-1,1,2048), getrelverschiebung(code, 1:numDat), data);
    colorbar
    colormap(jet)
    xlabel('$\tau$ [rel. Einheiten]')
    ylabel('$l$ [mm]')
    cbh = colorbar;
    set(cbh,'YTick',[0.2, 0.4, 0.6, 0.8, 1])
    ylim(cbh, [0.2, 1])
    ylabel(cbh,'$\log_{\max|S|}|S(\tau)|$', 'interpreter', 'latex')
    title('Messung mit BK7')
    


    code = '4-SF6';
    params = getSeriesParams(code);
    numDat = cell2mat(params(1));
    data = abs(mapFourier(code));
    data = log(data)/max(log(data(:)));
    
    subplot(3,1,3)
    yyaxis right
    ylim([0 numDat])
    set(gca, 'Ydir', 'reverse')
    ylabel('Nummer der Messung')
    yyaxis left
    imagesc(linspace(-1,1,2048), getrelverschiebung(code, 1:numDat), data);
    colorbar
    colormap(jet)
    xlabel('$\tau$ [rel. Einheiten]')
    ylabel('$l$ [mm]')
    ylabel(colorbar,'$\log_{\max|S|}|S(\tau)|$', 'interpreter', 'latex')
    
    title('Messung mit SF6')
    
    print('images/fourierzusammen.pdf', '-dpdf')
    
%% PRO exemplarisch alles durchgehen
    code = '4-BK7';
    number = 15;
    figure('Units','centimeters', 'Position',[0 0 13.25 10],'PaperPositionMode','auto');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
    
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    delta = getDelta(code, number)/1000;
    messung = mapRaw(code);
    raw = messung(number, :)./1e4;
    fou = fft(raw, 2048, 2);
    mid = 331;
    sigma = 80;
    maxi = 20;
    x = 1:2048;
    cutter = exp(-((x-mid)/sigma).^10);
    cut = fou.*cutter;
    back = ifft(cut);
    phi = unwrap(angle(back));
    dphidnu = diff(phi)./diff(nus*1e12);
    ng = -physconst('Lightspeed')/(4*pi*dicke)*dphidnu-(delta/dicke-1);
    
    %set(gca,'Units','normalized','FontUnits','points','FontWeight','normal','FontSize',9,'FontName',axesFontName)
    
    subplot(2,2,1)
    
    plot(lambs, raw)
    xlim([ill, irl])
    ylim([-0.3, 6])
    xlabel('$\lambda_0$ [nm]')
    ylabel('$S(\lambda_0)$ [rel. Einh.]')
    text(0,1.15,'a) gemessenes Spektrum','Units', 'Normalized', 'VerticalAlignment', 'Top')
    set(gca, 'FontSize', 8)
    subplot(2,2,2)
    plot(linspace(-1,1, length(fou)), abs(fftshift(fou)), 'r')
    xlim([-0.6, 0.6])
    ylim([-10, 200])
    x = gca;
    x.XTick = [-0.6, -0.3, 0, 0.3, 0.6];
    xlabel('$\tau$ [rel. Einh.]')
    ylabel('$\vert S(\tau)\vert$ [rel. Einh.]')
    text(0,1.15,'b) Fouriertransformierte','Units', 'Normalized', 'VerticalAlignment', 'Top')
    set(gca, 'FontSize', 8)
    
    p = subplot(2,2,3);
    plot(NaN)
    hold all
    ax = linspace(0,2, length(fou));
    a = plot(ax , abs(fou)/maxi, 'r');
    b = plot(ax , cutter, 'g');
    c = plot(ax , abs(cut)/maxi, 'b');
    hold off
    xlim([0, 0.600])
    ylim([0, 3])
    xlabel('$\tau$ [rel. Einh.]')
    ylabel('$\vert S(\tau)\vert$ [rel. Einh.]')
    legend([a, b, c], '$|S(\tau)|$', 'Filterfunktion', '$|S_{cut}(\tau)|$')
    text(0,1.15,'c) Filtervorgang','Units', 'Normalized', 'VerticalAlignment', 'Top')
    set(gca, 'FontSize', 8)
    
    subplot(2,2,4)
    plot(nus(2:end), ng)
    xlim([iln, irn])
    ylim([1.5, 1.65])
    xlabel('$\nu$ [THz]')
    ylabel('$N(\nu)$')
    text(0,1.15,'d) Gruppenbrechungsindex','Units', 'Normalized', 'VerticalAlignment', 'Top')
    set(gca, 'FontSize', 8)
    
    print('images/exemplarisch.pdf', '-dpdf')
    print('images/exemplarisch.eps', '-depsc2')
%% N 4-BK7

    code = '4-BK7';

    %figure('Units','centimeters', 'Position',[0 0 13.25 10],'PaperPositionMode','auto');
    measurements = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,35,36,37,38,39,40,41,42,43,44,45,46];
    mids = [499,474,448,421,407,382,360,333,309,273,250,229,203,170,150,184,207,228,253,280,311,336,360,382,413,434,461];
    sigmas = [80,80,80,80,80,80,80,80,80,80,80,80,80,60,50,50,50,50,50,50,60,65,70,70,80,60,70];
    maxs = [4e4, 5e4,6.4e4,8e4,9.4e4,1.1e5,1.36e5,1.55e5,1.9e5,2.4e5,3e5,3.7e5,4.7e5,6.5e5,8e5,4e5,4.5e5,3.7e5,3e5,2.3e5,1.8e5,1.6e5,1.3e5,1.2e5,1e5,7e4,5.5e4];
    vorzeichen = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1];

    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    plot(NaN)
    hold all
    a = plot(nus(2:end), mittel, 'LineWidth',1);
    b = plot(nus(2:end), mittel+abweichung, 'Color', [0.9290    0.6940    0.1250]);
    plot(nus(2:end), mittel-abweichung, 'Color', [0.9290    0.6940    0.1250]);
    d = plot(tbk7nus, tbk7, 'b');
    plot(nus, tbk7interpol)
    hold off
    xlim([340, 410])
    ylim([1.51, 1.6])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    legend([a, b, d], 'Mittelwert der Messungen', 'Konfidenzintervall', 'theoretischer Wert', 'Location','northeast');
    print -depsc2 images/Nbk7.eps
    figure
    quasifehler = (mittel-tbk7interpol(2:end))./abweichung;
    plot(nus(2:end), quasifehler)
    
    
 %% N 4-SF6
 
     code = '4-SF6';
    
    figure('Units','centimeters', 'Position',[0 0 13.25 10],'PaperPositionMode','auto');
    measurements = [11, 12, 13, 14, 15, 16, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
    mids = [430, 421, 394, 369, 354, 331, 256, 280, 300, 342, 366, 390, 425, 443, 460, 495, 522];
    sigmas = [160, 180, 200, 200, 200, 200, 100, 90, 80, 70, 65, 65, 65, 65, 65, 60, 55];
    maxs = [3e4, 4e4, 4.1e4, 5.5e4, 5.5e4, 6.4e4, 1.6e5, 1.5e5, 1.337e5, 1.1e5, 1e5, 8.9e4, 7e4, 6e4, 5e4, 4.4e4, 3.7e4];
    vorzeichen = [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    plot(NaN)
    hold all
    a = plot(nus(2:end), mittel, 'r', 'LineWidth',1);
    b = plot(nus(2:end), mittel+abweichung, 'Color', [0.9290    0.6940    0.1250]);
    plot(nus(2:end), mittel-abweichung, 'Color', [0.9290    0.6940    0.1250]);
    d = plot(tsf6nus, tsf6, 'b');
    hold off
    xlim([340, 410])
    ylim([1.815, 1.875])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    legend([a, b, d], 'Mittelwert der Messungen', 'Konfidenzintervall', 'theoretischer Wert', 'Location','northwest');

    print -depsc2 images/Nsf6.eps
    
%% PRO N zusammen

    code = '4-BK7';

    figure('Units','centimeters', 'Position',[0 0 13.25 20],'PaperPositionMode','auto');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
    
    
    measurements = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,35,36,37,38,39,40,41,42,43,44,45,46];
    mids = [499,474,448,421,407,382,360,333,309,273,250,229,203,170,150,184,207,228,253,280,311,336,360,382,413,434,461];
    sigmas = [80,80,80,80,80,80,80,80,80,80,80,80,80,60,50,50,50,50,50,50,60,65,70,70,80,60,70];
    maxs = [4e4, 5e4,6.4e4,8e4,9.4e4,1.1e5,1.36e5,1.55e5,1.9e5,2.4e5,3e5,3.7e5,4.7e5,6.5e5,8e5,4e5,4.5e5,3.7e5,3e5,2.3e5,1.8e5,1.6e5,1.3e5,1.2e5,1e5,7e4,5.5e4];
    vorzeichen = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1];
    
    achse = nus(2:end);
    
    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    %sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    
    
    subplot(2,1,1)
    
%     plot(NaN)
%     hold all
%     a = plot(achse, mittel, 'LineWidth',1);
%     b = plot(achse, mittel+abweichung, 'Color', [0.9290    0.6940    0.1250]);
%     plot(achse, mittel-abweichung, 'Color', [0.9290    0.6940    0.1250]);
%     d = plot(tbk7nus, tbk7, 'b');
%     hold off
    
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, abweichung, {'Color', [0.9290    0.6940    0.1250]});
    a2 = plot(achse, mittel, 'r', 'LineWidth',1);
    d = plot(tbk7nus, tbk7, 'b');
    hold off
    
    xlim([340, 410])
    ylim([1.51, 1.62])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    legend([a2, a.patch, d], 'Mittelwert der Messungen', 'Intervall der Standardabweichung', 'theoretischer Wert', 'Location','northeast');
    title('Gruppenbrechungsindex von BK7')
    
    code = '4-SF6';
    number = 11;
    
    measurements = [11, 12, 13, 14, 15, 16, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
    mids = [430, 421, 394, 369, 354, 331, 256, 280, 300, 342, 366, 390, 425, 443, 460, 495, 522];
    sigmas = [160, 180, 200, 200, 200, 200, 100, 90, 80, 70, 65, 65, 65, 65, 65, 60, 55];
    maxs = [3e4, 4e4, 4.1e4, 5.5e4, 5.5e4, 6.4e4, 1.6e5, 1.5e5, 1.337e5, 1.1e5, 1e5, 8.9e4, 7e4, 6e4, 5e4, 4.4e4, 3.7e4];
    vorzeichen = [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    %mittel2 = median(ergebnis);
    abweichung = std(ergebnis);
    subplot(2,1,2)
%     plot(NaN)
%     hold all
%     a = plot(achse, mittel, 'r', 'LineWidth',1);
%     %a2 = plot(nus(2:end), mittel2, 'g', 'LineWidth',1);
%     b = plot(achse, mittel+abweichung, 'Color', [0.9290    0.6940    0.1250]);
%     c = plot(achse, mittel-abweichung, 'Color', [0.9290    0.6940    0.1250]);
%     d = plot(tsf6nus, tsf6, 'b');
%     hold off
    
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, abweichung, {'Color', [0.9290    0.6940    0.1250]});
    a2 = plot(achse, mittel, 'r', 'LineWidth',1);
    d = plot(tsf6nus, tsf6, 'b');
    hold off
    
    xlim([340, 410])
    ylim([1.81, 1.85])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    legend([a2, a.patch, d], 'Mittelwert der Messungen', 'Intervall der Standardabweichung', 'theoretischer Wert', 'Location','northwest');
    title('Gruppenbrechungsindex von SF6')
    
    print -depsc2 images/Nzusammen.eps
    print('images/Nzusammen.pdf', '-dpdf')
    
%% PRO N fehlerdarstellung

    code = '4-BK7';

    figure('Units','centimeters', 'Position',[0 0 13.25 20],'PaperPositionMode','auto');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
    
    
    measurements = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,35,36,37,38,39,40,41,42,43,44,45,46];
    mids = [499,474,448,421,407,382,360,333,309,273,250,229,203,170,150,184,207,228,253,280,311,336,360,382,413,434,461];
    sigmas = [80,80,80,80,80,80,80,80,80,80,80,80,80,60,50,50,50,50,50,50,60,65,70,70,80,60,70];
    maxs = [4e4, 5e4,6.4e4,8e4,9.4e4,1.1e5,1.36e5,1.55e5,1.9e5,2.4e5,3e5,3.7e5,4.7e5,6.5e5,8e5,4e5,4.5e5,3.7e5,3e5,2.3e5,1.8e5,1.6e5,1.3e5,1.2e5,1e5,7e4,5.5e4];
    vorzeichen = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1];
    
    achse = nus(2:end);
    
    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    
    
    subplot(2,1,1)
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, sig, {'Color', [1.0000    0.6000    0.4000]});
    a2 = plot(achse, mittel, 'r');
    d = plot(tbk7nus, tbk7, 'b');
    hold off
    legend([a2, a.patch,d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    
    xlim([340, 410])
    %ylim([1.51, 1.62])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    
    title('Gruppenbrechungsindex von BK7')
    
    
    
    code = '4-SF6';
    number = 11;
    
    measurements = [11, 12, 13, 14, 15, 16, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
    mids = [430, 421, 394, 369, 354, 331, 256, 280, 300, 342, 366, 390, 425, 443, 460, 495, 522];
    sigmas = [160, 180, 200, 200, 200, 200, 100, 90, 80, 70, 65, 65, 65, 65, 65, 60, 55];
    maxs = [3e4, 4e4, 4.1e4, 5.5e4, 5.5e4, 6.4e4, 1.6e5, 1.5e5, 1.337e5, 1.1e5, 1e5, 8.9e4, 7e4, 6e4, 5e4, 4.4e4, 3.7e4];
    vorzeichen = [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end
    
    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    
    subplot(2,1,2)
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, sig, {'Color', [1.0000    0.6000    0.4000]});
    a2 = plot(achse, mittel, 'r');
    d = plot(tsf6nus, tsf6, 'b');
    hold off
    legend([a2, a.patch,d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    %legend([a, b, d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    xlim([340, 410])
    %ylim([1.81, 1.85])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    title('Gruppenbrechungsindex von SF6')
    
    print -depsc2 images/Nzusammenmitfehler.eps
    print('images/Nzusammenmitfehler.pdf', '-dpdf')
    
%% N fehlerdarstellung testen

    code = '4-BK7';

    %figure('Units','centimeters', 'Position',[0 0 13.25 20],'PaperPositionMode','auto');
    measurements = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,35,36,37,38,39,40,41,42,43,44,45,46];
    mids = [499,474,448,421,407,382,360,333,309,273,250,229,203,170,150,184,207,228,253,280,311,336,360,382,413,434,461];
    sigmas = [80,80,80,80,80,80,80,80,80,80,80,80,80,60,50,50,50,50,50,50,60,65,70,70,80,60,70];
    maxs = [4e4, 5e4,6.4e4,8e4,9.4e4,1.1e5,1.36e5,1.55e5,1.9e5,2.4e5,3e5,3.7e5,4.7e5,6.5e5,8e5,4e5,4.5e5,3.7e5,3e5,2.3e5,1.8e5,1.6e5,1.3e5,1.2e5,1e5,7e4,5.5e4];
    vorzeichen = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1];
    
    achse = nus(2:end);
    
    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    sigstd = abweichung;
    sigkap = sqrt(sigmakappasisq)/dicke;
    sigdic = sigd/dicke/dicke*(c0/4/pi*dphidnu-kappa);
    
    subplot(2,1,1)
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, sig, {'Color', [1.0000    0.6000    0.4000]});
    a2 = plot(achse, mittel, 'r');
    d = plot(tbk7nus, tbk7, 'b');
    plot(achse, mittel+sigstd)
    plot(achse, mittel+sigkap, 'y')
    plot(achse, mittel+sigdic, 'g')
    hold off
    legend([a2, a.patch,d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    
    xlim([340, 410])
    %ylim([1.51, 1.62])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    
    title('Gruppenbrechungsindex von BK7')
    
    
    
    code = '4-SF6';
    number = 11;
    
    measurements = [11, 12, 13, 14, 15, 16, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
    mids = [430, 421, 394, 369, 354, 331, 256, 280, 300, 342, 366, 390, 425, 443, 460, 495, 522];
    sigmas = [160, 180, 200, 200, 200, 200, 100, 90, 80, 70, 65, 65, 65, 65, 65, 60, 55];
    maxs = [3e4, 4e4, 4.1e4, 5.5e4, 5.5e4, 6.4e4, 1.6e5, 1.5e5, 1.337e5, 1.1e5, 1e5, 8.9e4, 7e4, 6e4, 5e4, 4.4e4, 3.7e4];
    vorzeichen = [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end
    
    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    sigstd = abweichung;
    sigkap = sqrt(sigmakappasisq)/dicke;
    sigdic = sigd/dicke/dicke*(c0/4/pi*dphidnu-kappa);
    
    subplot(2,1,2)
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, sig, {'Color', [1.0000    0.6000    0.4000]});
    a2 = plot(achse, mittel, 'r');
    d = plot(tsf6nus, tsf6, 'b');
    plot(achse, mittel+sigstd)
    plot(achse, mittel+sigkap, 'y')
    plot(achse, mittel+sigdic, 'g')
    hold off
    legend([a2, a.patch,d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    %legend([a, b, d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    xlim([340, 410])
    %ylim([1.81, 1.85])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    title('Gruppenbrechungsindex von SF6')
    
    print -depsc2 images/Nfehleranalyse.eps
    
%% N abweichung von theorie testen

    code = '4-BK7';

    %figure('Units','centimeters', 'Position',[0 0 13.25 20],'PaperPositionMode','auto');
    measurements = [8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,35,36,37,38,39,40,41,42,43,44,45,46];
    mids = [499,474,448,421,407,382,360,333,309,273,250,229,203,170,150,184,207,228,253,280,311,336,360,382,413,434,461];
    sigmas = [80,80,80,80,80,80,80,80,80,80,80,80,80,60,50,50,50,50,50,50,60,65,70,70,80,60,70];
    maxs = [4e4, 5e4,6.4e4,8e4,9.4e4,1.1e5,1.36e5,1.55e5,1.9e5,2.4e5,3e5,3.7e5,4.7e5,6.5e5,8e5,4e5,4.5e5,3.7e5,3e5,2.3e5,1.8e5,1.6e5,1.3e5,1.2e5,1e5,7e4,5.5e4];
    vorzeichen = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1];
    
    achse = nus(2:end);
    
    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end

    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    sigstd = abweichung;
    sigkap = sqrt(sigmakappasisq)/dicke;
    sigdic = sigd/dicke/dicke*(c0/4/pi*dphidnu-kappa);
    
    subplot(2,1,1)
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, sig, {'Color', [1.0000    0.6000    0.4000]});
    a2 = plot(achse, mittel, 'r');
    d = plot(tbk7nus, tbk7, 'b');
    plot(achse, abs(mittel./tbk7interpol(2:end)-1))
    hold off
    legend([a2, a.patch,d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    
    %xlim([340, 410])
    %ylim([1.51, 1.62])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    
    title('Gruppenbrechungsindex von BK7')
    
    
    
    code = '4-SF6';
    number = 11;
    
    measurements = [11, 12, 13, 14, 15, 16, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
    mids = [430, 421, 394, 369, 354, 331, 256, 280, 300, 342, 366, 390, 425, 443, 460, 495, 522];
    sigmas = [160, 180, 200, 200, 200, 200, 100, 90, 80, 70, 65, 65, 65, 65, 65, 60, 55];
    maxs = [3e4, 4e4, 4.1e4, 5.5e4, 5.5e4, 6.4e4, 1.6e5, 1.5e5, 1.337e5, 1.1e5, 1e5, 8.9e4, 7e4, 6e4, 5e4, 4.4e4, 3.7e4];
    vorzeichen = [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

    fourierAll = mapFft(code);
    params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    ergebnis = [];
    for count = 1:length(measurements)
        number = measurements(count);
        delta = getDelta(code, number)/1000;
        kappa = getkappa(code, number)/1000;
        fourier = fourierAll(number,:);

        mid = mids(count);
        sigma = sigmas(count);
        maxi = maxs(count);

        fourier = fourier./maxi;
        x = 1:2048;
        cutter = exp(-((x-mid)/sigma).^10);
        cut = fourier.*cutter;
        backf = ifft(cut);
        wphi = angle(backf);
        phi = unwrap(wphi);
        dphidnu = diff(phi)./diff(nus*1e12);
        %ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);
        ng = vorzeichen(count)*physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(kappa/dicke-1);
        ergebnis = [ergebnis; ng]; %#ok<AGROW>
    end
    
    mittel = mean(ergebnis);
    abweichung = std(ergebnis);
    sigd = getsigmad(code)/1000;
    sig = sqrt(abweichung.^2+sigmakappasisq/dicke^2+(sigd/dicke)^2/dicke^2*(c0/4/pi*dphidnu-kappa).^2);
    sigstd = abweichung;
    sigkap = sqrt(sigmakappasisq)/dicke;
    sigdic = sigd/dicke/dicke*(c0/4/pi*dphidnu-kappa);
    
    subplot(2,1,2)
    plot(NaN)
    hold all
    a = shadedErrorBar(achse, mittel, sig, {'Color', [1.0000    0.6000    0.4000]});
    a2 = plot(achse, mittel, 'r');
    d = plot(tsf6nus, tsf6, 'b');
    plot(achse, abs(mittel./tsf6interpol(2:end)-1))
    hold off
    legend([a2, a.patch,d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    %legend([a, b, d], 'Mittelwert der Messungen', 'Fehlerintervall', 'theoretischer Wert', 'Location','northwest');
    %xlim([340, 410])
    %ylim([1.81, 1.85])
    xlabel('$\nu$ [THz]')
    ylabel('$N$')
    title('Gruppenbrechungsindex von SF6')
    
    print -depsc2 images/Nfehleranalyse.eps
    