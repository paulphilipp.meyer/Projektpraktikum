#!/bin/bash

# durchsucht aktuelles und untergeordnete verzeichnisse nach
# .txt-dateien und ersetzt darin alle kommata durch punkte

# echo files
#find . -name "*.txt" | while read file; do echo "$file"; done

#replace , with .
find . -name "*.txt" | while read file; do sed -i 's/,/\./g' "$file"; done
