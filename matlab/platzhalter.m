code = '6-0';
phase = mapTempDiffPhi(code);
%plot(phase(23,:))
imagesc(mapTempDiffPhi(code))

figure(2)
code = '6-0';
number = 30;
raw = mapRaw(code);
imagesc(raw)
plot(raw(number, :));

subplot(2,1,1)
imagesc(abs(fft(raw, 2048, 2)))
subplot(2,1,2)
imagesc(abs(fftshift(fft(raw, 2048, 2), 2)))

fouriers = mapFourierSchnitt(code);
imagesc(abs(fouriers))

back = mapRuecktrafo(code);
imagesc(abs(back));
plot(real(back(number, :)))
