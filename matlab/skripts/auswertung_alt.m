%% Interferogramme darstellen
close all
clear all
clc

% Parameter
    code = '3-0';   % available Series: getAvailableSeries()
    filename = '';  % leer lassen, um nicht zu Speichern

% get file parameters
    [numLastDat, missingFiles, filePrefix] = measureSeriesFileParams(code);
% load data
    [meas, lambs, intens] = readSeries(numLastDat, missingFiles, filePrefix);

    minLamb = 730;
    maxLamb = 870;
    minn = 10;
    maxn = 45;
    cprintInterspektrogramm(code, meas, lambs, intens, minLamb, maxLamb)%, minn, maxn)

%% Zeige normiertes Interspektrogramm

% Parameter
    messreihe = '4-SF6';

% Daten lesen
    lambs = getLambs();
    
    a = normalizedSpectrum(messreihe);
    imagesc(lambs(1260:1610), 1:size(a,1), a(:,1260:1610));
    
%% Spektrale Leistungsdichte Laserpuls
% zum herausfinden, welche messungen einer messreihe geeignet sind um die
% spektrale leistungsdichte des lasers ohne interferenz zu charakterisieren
    close all
    clear all
    clc
   
% Messreihen einlesen
    messreihe = '4-SF6';
    usableMeas = [1:6, -8:-20];
    
% daten lesen
    lambs = getLambs();
    messreihen = [];
    numMeas = 0;
    
    for number = usableMeas
        intens = readMeasurement(messreihe, number);
        messreihen = [messreihen; intens]; %#ok<*AGROW>
        numMeas = numMeas + 1;
    end

% Darstellen
    imagesc(lambs, 1:numMeas, messreihen);

%% Auswertung mittels einzelner Spektrogramme
serie = '6-BK7';
number = 20;
nfourier = 2^11;
leftBorder = 1176;
rightBorder = 1230;

close all;


lambs = getLambs();
a = readMeasurement(serie, number);

figure;kouzhh
plot(a);
title(sprintf('%s - Messung %i\nSpektrum', serie, number));

a_2 = fft(a);
a_3 = fftshift(a_2);

figure;
plot(abs(a_3));
title(sprintf('%s - Messung %i\nFouriertrafo', serie, number));



a_4 = [zeros(1, leftBorder-1), a_3(:,leftBorder:rightBorder), zeros(1, size(a_3, 2)-rightBorder)];

figure;
plot(abs(a_4));
title(sprintf('%s - Messung %i\nFouriertrafo - Ausschnitt', serie, number));

a_5_1 = ifftshift(a_4);
a_5 = ifft(a_5_1);

% figure;
% plot(abs(a_5));
% title(sprintf('%s - Messung %i\nZurücktransformierter Peak', serie, number));

a_6 = angle(a_5);

figure;
plot(abs(a_6));
title(sprintf('%s - Messung %i\nPhase', serie, number));

a_7 = unwrap(a_6);

figure;
plot(abs(a_7));
title(sprintf('%s - Messung %i\nPhase - unwrapped', serie, number));
%n = normalizedSpectrum(serie);
%n_2 = fft(n);
%n_3 = fftshift(n_2);

%% Fouriertrafo von Serie
m = readSeriesSimple('6-SF6');
m_2 = fft(m, 10000, 2);
m_3 = fftshift(m_2, 2);
m_4 = m_3(:,1:40000);
figure;

imagesc(abs(m_4));
title('6-SF6');
imagesc(abs(m_3));

