%% alles clean neu laden
close all
clear variables
clc

load('data2/constants')
load('data2/klotz')



%% set defaults
set(groot, 'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex')
set(groot, 'defaultLegendInterpreter','latex');
set(groot, 'defaultAxesFontName', 'Times')
set(groot, 'defaultUicontrolFontName', 'Times')
set(groot, 'defaultUitableFontName', 'Times')
set(groot, 'defaultAxesFontName', 'Times')
set(groot, 'defaultTextFontName', 'Times')
set(groot, 'defaultUipanelFontName', 'Times')
set(groot,'defaultAxesColorOrder',[0 0 0; 0 0 0]);

%% zeige Einzelmessung
code = '6-SF6';
number = 40;
xAchse = nus;        % lambs / nus
cut = 1;               % 1 / 0
data = mapRaw(code);

deltas = mapDelta(code);

if cut
    plot(xAchse(hleft:hright), data(number, hleft:hright))
else
    plot(xAchse, data(number, :)) %#ok<*UNRCH>
end

title(sprintf('%s, n° %i\nDelta: %0.2f mm', code, number, deltas(number)))

%% zeige Gruppen Index

code = '6-SF6';
number = 10;


ngs = mapGroupIndex(code);

figure(2)
plot(nus(hleft:hright), ngs(number, hleft:hright))
title(sprintf('Gruppenindex von %s n°%i', code, number))

%% zeige phi

code = '6-SF6';
number = 20;


dphi = mapDiffPhi(code);

figure(2)
plot(nus(hleft:hright), dphi(number, hleft:hright))
title(sprintf('d Phase / d nu von %s n°%i', code, number))

%% zeige verlauf
load('data2/klotz')


code = '6-SF6';
number = 10;

% spektrum
graw = mapRaw(code);
geraw = graw(number, :);

plot(nus(hleft:hright), geraw(hleft:hright));

% fourier
gfourier = mapFourier(code);
gefourier = gfourier(number, :);

plot(abs(gefourier));