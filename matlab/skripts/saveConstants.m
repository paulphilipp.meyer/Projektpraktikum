pathkeys = {'constants', 'klotz'};
paths = {'constants', 'data2/klotz'};
filepaths = containers.Map(pathkeys, paths);

c = matfile(cell2mat(paths(1)),'Writable',true);

c.tsf6lambs = [880, 850, 840, 830, 820, 810, 800, 760, 750, 720, 700, 710, 730, 740, 770, 780, 790, 860, 870, 890, 900];
c.tsf6lambs = sort(c.tsf6lambs);
c.tsf6nus = lamb2nu(c.tsf6lambs);
c.tsf6 = [1.8204, 1.8244, 1.8258, 1.8274, 1.8289, 1.8306, 1.8323, 1.8400, 1.8421, 1.8492, 1.8546, 1.8518, 1.8467, 1.8444, 1.8379, 1.8360, 1.8341, 1.8230, 1.8217, 1.8191, 1.8179];
c.tsf6 = sort(c.tsf6, 'descend');
c.tsf6interpol = interp1(c.tsf6lambs, c.tsf6, getLambs());

c.tbk7lambs = [720, 740, 760, 780, 800, 820, 840, 860, 880, 710, 730, 750, 770, 790, 810, 830, 850, 870, 890, 700, 900];
c.tbk7lambs = sort(c.tbk7lambs);
c.tbk7nus = lamb2nu(c.tbk7lambs);
c.tbk7 = [1.5305, 1.5294, 1.5284, 1.5275, 1.5266, 1.5259, 1.5252, 1.5246, 1.524, 1.5311, 1.5299, 1.5289, 1.5279, 1.5271, 1.5263, 1.5255, 1.5249, 1.5243, 1.5237, 1.5317, 1.5235];
c.tbk7 = sort(c.tbk7, 'descend');
c.tbk7interpol = interp1(c.tbk7lambs, c.tbk7, getLambs());

c.c0 = physconst('LightSpeed');
c.sigmakappasisq = 0.00005^2+0.00001^2;

c.filepaths = filepaths;
c.lambs = getLambs();
c.nus = lamb2nu(c.lambs);
c.stepwidth = 0.01; % schritteweite spiegelversatz [mm]

% Darstellungsgrenzen
c.ll = 700;
c.rl = 900;
c.ln = lamb2nu(ll);
c.rn = lamb2nu(rl);

% interessanter Spektralbereich
c.hll = 736; % linke grenze verwertbarer daten (lambda)
c.hrl = 864; % rechte grenze verwertbarer daten (lambda)
c.hlnl = indexFromValue(lambs, hll); % index linke verwertbare grenze
c.hrnl = indexFromValue(lambs, hrl); % index rechte verwertbare grenze

c.hln = round(lamb2nu(c.hrl)); % linke grenze verwertbarer daten (nu)
c.hrn = round(lamb2nu(c.hll)); % rechte grenze verwertbarer daten (nu)
c.hlnn = indexFromValue(c.nus, c.hln, 1); % index linke verwertbare grenze
c.hrnn = indexFromValue(lambs, hrl); % index rechte verwertbare grenze

c.hleft = indexFromValue(lambs, hll);
c.hright = indexFromValue(lambs, hrl);


% tests
%{
disp(sprintf('\nziel: %i', c.hln))
disp(c.nus(1, c.hlnn+1))
disp(c.nus(1, c.hlnn))
disp(c.nus(1, c.hlnn-1))
%}