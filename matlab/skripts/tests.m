%{
unregelmaessig verteilte datenpunkte plotten:
	http://www.mathworks.com/examples/matlab/community/14125-example

liste rückwärts:
	10:-1:1;

länge von vektoren:
	length(v);

teile von matrizen löschen:
	m(:,2) = []

funtionen die variable anzahl von werten zurückgeben:
	fzero
%a=num2str(6, '%02d')

%}
%{
a = peaks(10);
b=1:10;
c=1:10;
surf(b,c, a)
%}

%ismember(4, 1:4)
%a=[4,]
%ismember(4, a)

%{
a=rand(5,5) %#ok<NOPTS>
b=repmat(double([2. 1. 2. 100. 2.]), 5, 1)
a./b
%}

%%
n=4;
m=5;
b = reshape(1:n*m, m, n).';
imagesc(b)

%% teste datenspeichern
code = '6-SF6';
schnitt = mapFourierSchnitt(code);
schnitt40 = schnitt(40, :);
plot(abs(schnitt40))


%% prüfe auf fehler in spiegelverschiebung
for serie = getAvailableSeries()
    params = getSeriesParams(cell2mat(serie));
    borders = cell2mat(params(5));
    distance = borders(1) - borders(2);
    numDat = cell2mat(params(1));
    weite = distance/(numDat-1);
    disp(serie);
    disp([distance, numDat, weite, distance-(numDat-1)*0.01]);
end

%% count lambs
disp('started')
availables = getAvailableSeries();
for serie = availables();
    disp(serie)
    [lambs,~] = readMeasurement(char(serie),1);
    disp(size(lambs, 1))
end


%% Laserspektrum 4-0
code = '4-SF6';
number = 37;

%spectrum = spectralDensity('4-0');
%spectrum(spectrum < 300) = 0;
mparams = getMeasParams(code, number);

load('constants');
%fspectrum = fftshift(fft(spectrum));
%plot(abs(fspectrum));


measure0 = readMeasurement(code, number);
%norm0 = measure0./spectrum;
fmeasure0 = fftshift(fft(measure0));
%fnorm0 = fftshift(fft(norm0));

schnitt = zeros(1, length(fmeasure0));
% % 4-BK7, 34
% left = 1154;
% right = 1224
% % 4-0, 34
% left = 1089;
% right = 1121;
% 4-SF6, 34
% left = 1045;
% right = 1193;
%4-SF6, 36
% left = 1069;
% right = 1262;
% allgemein:
borders = deal(cell2mat(mparams(1)));
if length(borders) < 2
    error('grenzen zu %s, %i noch nicht eingetragen', code, number')
end
left = borders(1);
right = borders(2);

schnitt(left:right) = fmeasure0(left:right);

zurueck = ifft(ifftshift(schnitt));

phi = unwrap(angle(zurueck));
cutphi = nan(1,length(phi));
cutphi(hlnl:hrnl) = phi(hlnl:hrnl);
%scutphi = smooth(cutphi, 'loess').';
dphi = diff(phi)./diff(nus);
dcutphi = diff(cutphi)./diff(nus);

%% display messung - phi
fig = figure;
fig.Name = sprintf('%s - N° %i', code, number);
fig.NumberTitle = 'off';
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.7 0 0.3 1]);

subplot(4,1,1);
plot(lambs, measure0)
title(sprintf('%s, %i \n gemessen', code, number))
xlim([ll, rl]);
    subplot(4,1,2)
    plot(abs(fmeasure0))
    title('fourier')
subplot(4,1,3)
plot(abs(schnitt))
title('schnitt')
% subplot(4,1,3)
% plot(lambs, norm0)
% title('normiert')
    subplot(4,1,4)
    plot(lambs, phi)
    title('phi')
    xlim([ll, rl]);
subplot(4,1,3)
plot(lambs, real(zurueck))
title('zurücktransformiert (Realteil)')
xlim([ll, rl]);

%% display phi, diff
fig = figure;
fig.Name = sprintf('%s - N° %i', code, number);
fig.NumberTitle = 'off';
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.7 0 0.3 1]);

    subplot(4,1,1);
    plot(nus, cutphi)
    title(sprintf('%s, %i \n cutphi(nu)', code, number))
    xlim([rn, ln]);
    %ylim([170, 300]);

subplot(4,1,2)
plot(lambs, cutphi)
title('cutphi(lambda)')
xlim([ll, rl]);
    
subplot(4,1,4)
plot(nus(2:end), dcutphi)
title('d/dnu (cutphi)')
xlim([ln, rn]);
ylim([0, 3]);

%% Konstanten laden
load('constants')

%% grenzen des fourierbuckels bestimmen

code = '6-0';
number = 30;

figureNum = 7;

% initialisieren
%     lambs = getLambs();
%     ll = 700; % darstellungsgrenzen
%     rl = 900;
%     ln = lamb2nu(ll);
%     hll = 736; % linke grenze verwertbarer daten (lambda)
%     hrl = 864; % rechte grenze verwertbarer daten (lambda)
%     hln = indexFromValue(lambs, hll); % index linke verwertbare grenze
%     hrn = indexFromValue(lambs, hrl); % index rechte verwertbare grenze
%     
%     fig = figure(figureNum);
%     fig.Name = sprintf('%s - N° %i', code, number);
%     fig.NumberTitle = 'off';
%     set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.6 0 0.4 1]);

% rechnen
    mparams = getMeasParams(code, number);
    measure0 = readMeasurement(code, number);
    fmeasure0 = fftshift(fft(measure0));

% plot messwerte
    subplot(4,1,1)
    plot(lambs, measure0)
    title(sprintf('%s, %i \n Messwerte', code, number))
    xlim([ll, rl]);

% plot fourier
    subplot(4,1,2)
    plot(abs(fmeasure0))
    title('Fouriertrafo')
   
% grenzen
    borders = deal(cell2mat(mparams(1)));
    if length(borders) < 2
        error('grenzen zu %s, %i eintragen', code, number')
    end
    left = borders(1);
    right = borders(2);


% rechnen
    schnitt = zeros(1, length(fmeasure0));
    schnitt(left:right) = fmeasure0(left:right);

    zurueck = ifft(ifftshift(schnitt));

    phi = unwrap(angle(zurueck));
    cutphi = nan(1,length(phi));
    cutphi(hln:hrn) = phi(hln:hrn);
    
% plot rücktrafo
    subplot(4,1,3)
    plot(lambs, real(zurueck))
    title('zurücktransformiert (Realteil)')
    xlim([ll, rl]);

% plot phi  
    subplot(4,1,4)
    plot(lambs, phi)
    title('phi')
    xlim([ll, rl]);
    
%% beispiel ropers

    fig = figure(3);
    fig.Name = sprintf('%s - N° %i', code, number);
    fig.NumberTitle = 'off';
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.7 0 0.3 1]);
    
    subplot(3,1,1)
    plot(lambs, measure0)
    title(sprintf('%s, %i \n Messwerte', code, number))
    xlim([ll, rl]);
   
% plot phi  
    subplot(3,1,2)
    plot(lambs, cutphi)
    title('Phase')
    xlim([ll, rl]);
    
    subplot(3,1,3)
    plot(lambs(2:end), diff(cutphi))
    title('Ableitung der Phase')
    xlim([ll, rl]);
    
%% display phase und ableitung

code = '6-SF6';
number = 20;
xAchse = nus;        % lambs / nus
data = mapRaw(code);

phis = mapPhaseUncut(code);
dphis = mapDiffPhi(code);

subplot(2,1,1)
plot(nus, phis(number, :))
title(sprintf('%s, n° %i', code, number))
xlim([rn, ln])
subplot(2,1,2)
plot(nus(2:end), dphis(number, :))
xlim([rn, ln])

%% nächster versuch
%% schnittstellen bestimmen

code = '4-BK7';
%number = 44;
%number = 37;
% code = '6-BK7';
% number = 12;

number = 15;

params = getSeriesParams(code);
dicke = cell2mat(params(6))/1000;
delta = getDelta(code, number)/1000;
messung = mapRaw(code);
fourierAll = mapFft(code);
fourier = fourierAll(number,:);

subplot(2,1,1)
imagesc(log(abs(fourierAll)))
colormap(jet)

subplot(2,1,2)
plot(abs(fourier))
title(sprintf('%s, n°%i', code, number))

%%
% mid =  407;
% sigma = 60;
% max = 7e4;

% mid =  220;
% sigma = 100;
% max = 2e5;

% mid =  236;
% sigma = 50;
% max = 8e4;

mid = 332;
sigma = 80;
max = 3e5;

fourier = fourierAll(number,:);
fourier = fourier./max;



x = 1:2048;
cutter = exp(-((x-mid)/sigma).^10);
cut = fourier.*cutter;
backf = ifft(cut);
wphi = angle(backf);
phi = unwrap(wphi);
dphidnu = diff(phi)./diff(nus*1e12);

subplot(2,1,1)
plot(NaN) 
hold on
plot(abs(fourier))
ylim([-0.2, 1.2])
xlim([mid-1.5*sigma, mid+1.5*sigma])
plot(cutter)
plot(abs(cut))
hold off

subplot(2,1,2)

plot(lambs(2:end), dphidnu)
%plot(phi)
title(sprintf('d phi / dnu %s, n°%i', code, number))
%xlim([hleft, hright])

% gruppenindex
%delta = -0.000674;
ng = -physconst('Lightspeed')/(4*pi*dicke)*dphidnu-1*(delta/dicke-1);

plot(NaN)
hold on
plot(nus(2:end), ng)
title(sprintf('gruppenindex %s, n°%i', code, number))
if (code(3) == 'B')
    plot(tbk7nus, tbk7)
elseif (code(3) == 'S')
    plot(tsf6nus, tsf6)
end
%ylim([1.5, 1.7])
%xlim([700, 900])
hold off
%% theoretischer Gruppenindex SF6
tsf6lambs = [880, 850, 840, 830, 820, 810, 800, 760, 750, 720];
tsf6nus = lamb2nu(tnglambs);
tsf6 = [1.8204, 1.8244, 1.8258, 1.8274, 1.8289, 1.8306, 1.8323, 1.8400, 1.8421, 1.8492];
plot(tnglambs, tng);

%% theoretischer Gruppenindex BK7
tbk7lambs = [720, 740, 760, 780, 800, 820, 840, 860, 880];
tbk7nus = lamb2nu(tbk7lambs);
tbk7 = [1.5305, 1.5294, 1.5284, 1.5275, 1.5266, 1.5259, 1.5252, 1.5246, 1.524];
plot(tbk7lambs, tbk7)

%% plotplazierung

set(gcf, 'Units','centimeters', 'Position',[20 5 13.25 10],'PaperPositionMode','auto');
set(gcf,'PaperPositionMode','manual');
set(gcf,'PaperPosition',[0 0 10 12]);

params = getSeriesParams(code);
    dicke = cell2mat(params(6))/1000;
    delta = getDelta(code, number)/1000;
    messung = mapRaw(code);
    raw = messung(number, :)./1e4;
    fou = fft(raw, 2048, 2);
    mid = 331;
    sigma = 80;
    maxi = 20;
    x = 1:2048;
    cutter = exp(-((x-mid)/sigma).^10);
    cut = fou.*cutter;
    back = ifft(cut);
    phi = unwrap(angle(back));
    dphidnu = diff(phi)./diff(nus*1e12);
    ng = -physconst('Lightspeed')/(4*pi*dicke)*dphidnu-(delta/dicke-1);
    
    %set(gca,'Units','normalized','FontUnits','points','FontWeight','normal','FontSize',9,'FontName',axesFontName)
    
    subplot(2,2,1)
    
    plot(lambs, raw)
    xlim([ill, irl])
    ylim([-0.3, 6])
    xlabel('$\lambda_0$ [nm]')
    ylabel('$S(\lambda_0)$ [rel. Einh.]')
    text(0,1.15,'a) gemessenes Spektrum','Units', 'Normalized', 'VerticalAlignment', 'Top')
    set(gca, 'FontSize', 8)
    subplot(2,2,2)
    plot(linspace(-1,1, length(fou)), abs(fftshift(fou)), 'r')
    xlim([-0.6, 0.6])
    ylim([-10, 200])
    x = gca;
    x.XTick = [-0.6, -0.3, 0, 0.3, 0.6];
    xlabel('$\tau$ [rel. Einh.]')
    ylabel('$\vert S(\tau)\vert$ [rel. Einh.]')
    text(0,1.15,'b) Fouriertransformierte','Units', 'Normalized', 'VerticalAlignment', 'Top')
    set(gca, 'FontSize', 8)