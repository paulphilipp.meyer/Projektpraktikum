%% spiegelverschuebe

mapDelta = containers.Map();
for code = {'6-0', '6-BK7', '6-SF6'}
    code = cell2mat(code); %#ok<FXSET>
    params = getSeriesParams(code);
    numDats = cell2mat(params(1));
    
    delt = nan(numDats, 1);
    for number = 1:numDats
        delt(number) = getDelta(code, number);
    end
    
    mapDelta(code) = delt;
end

save('data2/klotz', 'mapDelta', '-append') % '-v7.3',

% %% rohdaten
% 
% mapRaw = containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     
%     raw = nan(numDats, 2048);
%     
%     for number = 1:numDats
%         mea = readMeasurement(code, number);
%         raw(number, :) = mea;
%     end
%     
%     mapRaw(code) = raw;
% end
% 
% save('data2/klotz', 'mapRaw', '-append')

%% rohdaten via readSeriesSimple

mapRaw = containers.Map();
for code = {'4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'}
    code = cell2mat(code); %#ok<FXSET>
    params = getSeriesParams(code);
    numDats = cell2mat(params(1));
    
    raw = nan(numDats, 2048);
    
    for number = 1:numDats
        mea = readMeasurement(code, number);
        raw(number, :) = mea;
    end
    
    mapRaw(code) = raw;
end

save('data2/klotz', 'mapRaw', '-append')

%% fouriertransformierte

load('data2/klotz', 'mapRaw');

mapFourier = containers.Map();
for code = {'4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'}
    code = cell2mat(code); %#ok<FXSET>
    params = getSeriesParams(code);
    numDats = cell2mat(params(1));
    
    fraw = fftshift(fft(mapRaw(code), 2048, 2), 2);
    
    
    mapFourier(code) = fraw;
end

save('data2/klotz', 'mapFourier', '-append')

% %% geschnitten
% 
% load('data2/klotz', 'mapFourier');
% 
% mapFourierSchnitt = containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     disp(fprintf('Serie: %s\n', code))
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     geschnitten = zeros(numDats, 2048);
%     for number = 1:numDats
%         disp(fprintf('Messreihe %i\n', number))
%         mparams = getMeasParams(code, number);
%         if isequal(mparams(1), {[]})
%             continue
%         else
%             singlefourier = mapFourier(code);
%             borders = deal(cell2mat(mparams(1)));
%             if length(borders) < 2
%                 error('grenzen zu %s, %i noch nicht eingetragen', code, number')
%             end
%             left = borders(1);
%             right = borders(2);
%             geschnitten(number, left:right) = singlefourier(number, left:right);
%         end
%     end
%     
%     mapFourierSchnitt(code) = geschnitten;
% end
% 
% save('data2/klotz', 'mapFourierSchnitt', '-append')

% %% rücktransformiert
% 
% load('data2/klotz', 'mapFourierSchnitt');
% 
% mapRuecktrafo = containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     
%     fback = ifft(ifftshift(mapFourierSchnitt(code), 2), 2048, 2);
%     
%     
%     mapRuecktrafo(code) = fback;
% end
% 
% save('data2/klotz', 'mapRuecktrafo', '-append')

% %% phase (ungeschnitten)
% 
% load('data2/klotz', 'mapRuecktrafo');
% 
% mapPhaseUncut = containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     disp(sprintf('Serie: %s', code)) %#ok<DSPS>
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     
%     phasen = unwrap(angle(mapRuecktrafo(code)), [], 2);
%     
%     mapPhaseUncut(code) = phasen;
% end
% 
% save('data2/klotz', 'mapPhaseUncut', '-append')

% %% phase (geschnitten)
% 
% load('data2/klotz', 'mapPhaseUncut');
% load('constants');
% 
% mapPhaseCut = containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     
%     phasen = mapPhaseUncut(code);
%     phasennullcut = nan(numDats, hright-hleft+1);
%     for number = 1:numDats
%         phasennullcut(number, :) = phasen(number, hleft:hright)-phasen(number, hleft);
%     end
%     mapPhaseCut(code) = phasennullcut;
% end
% save('data2/klotz', 'mapPhaseCut', '-append')

% %% ableitung d phi / d nu
% 
% load('data2/klotz', 'mapPhaseUncut');
% load('constants', 'nus');
% 
% mapDiffPhi = containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     
%     diffphi = diff(mapPhaseUncut(code), 1, 2)./repmat(diff(nus), numDats, 1);
%     
%     mapDiffPhi(code) = diffphi;
% end
% 
% save('data2/klotz', 'mapDiffPhi', '-append')

% %% group index
% 
% load('data2/klotz', 'mapPhaseCut');
% load('constants');
% 
% mapGroupIndex= containers.Map();
% for code = {'6-0', '6-BK7', '6-SF6'}
%     code = cell2mat(code); %#ok<FXSET>
%     params = getSeriesParams(code);
%     numDats = cell2mat(params(1));
%     
%     dphi = mapDiffPhi(code);
%     d = cell2mat(params(6));
%     nluft = repmat(nair(nus(2:end)), numDats, 1);
%     delta = repmat(getDelta(code, (1:numDats).'), 1, 2047);
%     ng = physconst('Lightspeed') * dphi*1e-12 ./ (4*pi*repmat(d/1000, numDats, 2047)) - nluft .* (delta/d - 1);
%     mapGroupIndex(code) = ng;
% end
% 
% save('data2/klotz', 'mapGroupIndex', '-append')

%% fourier ohne shift

load('data2/klotz', 'mapRaw');

mapFft = containers.Map();
for code = {'4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'}
    code = cell2mat(code); %#ok<FXSET>
    params = getSeriesParams(code);
    numDats = cell2mat(params(1));
    
    fraw = fft(mapRaw(code), 2048, 2);
    
    
    mapFft(code) = fraw;
end

save('data2/klotz', 'mapFft', '-append')


