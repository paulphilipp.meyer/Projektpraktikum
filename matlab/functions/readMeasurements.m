function intens = readMeasurements(code, measurements)
% READMEASUREMENTS
%   intens = readMeasurements(code, measurements
%   intens: measuremens x lambdas

intens = [];
for mea=measurements
    intens = [intens; readMeasurement(code, mea)]; %#ok<AGROW>
end
        