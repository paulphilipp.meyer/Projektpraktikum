function [lastDat, abscentDats, filePref] = measureSeriesFileParams(code)
% MEASURESERIESFILEPARAMS
%   
%   veraltet, benutze stattdessen getSeriesParams()
%
%   [numLastDat, abscentDats, filePref] = measureSeriesFileParams(code)
%   available Series:
%   '3-0', '3-SF6_hi', '4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'

if isequal(code, '1-0')
    lastDat = 27;
    abscentDats = [];
    filePref = 'data/First Try/20160603_Spektrum_without_SF6__USB2H003391_';
elseif isequal(code, '3-0')
    lastDat = 45;
    abscentDats = [6];
    filePref = 'data/Third Try/20160603_Spektrum_without_SF6_3rd_try__USB2H003391_';
elseif isequal(code, '3-SF6')
    lastDat = 45;
    abscentDats = [];
    filePref = 'data/Third Try with SF6/20160603_Spektrum_with_SF6_3rd_try__USB2H003391_';
elseif isequal(code, '3-SF6_hi')
    lastDat = 44;
    abscentDats = [];
    filePref = 'data/Third Try with SF6 higher Intensity/20160603_Spektrum_with_SF6_3rd_try_higher_Intensity_USB2H003391_';
elseif isequal(code, '4-0')
    lastDat = 62;
    abscentDats = [];
    filePref = 'data/Fourth Try/20160607_Spektrum_without_SF6_4th_try_USB2H003391_';
elseif isequal(code, '4-BK7')
    lastDat = 55;
    abscentDats = [];
    filePref = 'data/Fourth Try with BK7/20160607_Spektrum_with_BK7_4th_try_USB2H003391_';
elseif isequal(code, '4-SF6')
    lastDat = 62;
    abscentDats = [];
    filePref = 'data/Fourth Try with SF6/20160607_Spektrum_with_SF6_4th_try_USB2H003391_';
elseif isequal(code, '6-0')
    lastDat = 51;
    abscentDats = [];
    filePref = 'data/Sixth Try/20160610_Spektrum_without_6th_try_USB2H003391_';
elseif isequal(code, '6-BK7')
    lastDat = 45;
    abscentDats = [];
    filePref = 'data/Sixth Try with BK7/20160610_Spektrum_with_BK7_6th_try_USB2H003391_';
elseif isequal(code, '6-SF6')
    lastDat = 51;
    abscentDats = [];
    filePref = 'data/Sixth Try with SF6/20160610_Spektrum_with_SF6_6th_try_USB2H003391_';
end