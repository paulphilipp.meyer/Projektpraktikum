function nu = lamb2nu(lamb)
% LAMB2NU
% nu = lamb2nu(lamb)
% lamb [nm]
% nu [THz]
    nu = physconst('LightSpeed')*1e-3./(lamb);