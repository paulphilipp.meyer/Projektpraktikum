function delta = getDelta(code, number, zeropoint)
% GETDELTA
%   delta = getDelta(code, number, zero)
%   gibt die verschiebung in mm des Spiegels relativ zur Nullage wieder

% eingestellt auf nullmessung ohne probe



load('constants', 'stepwidth');
params = getSeriesParams(code);
borders = cell2mat(params(5));

if ~exist('zeropoint', 'var')
    %zero = borders(3);
    
    %null aus messung ohne probe 
    paramsnull = getSeriesParams([code(1), '-0']);
    bordersnull = cell2mat(paramsnull(5));
    zeropoint = bordersnull(3);  
end


position = borders(1) - stepwidth*(number-1);
delta = position - zeropoint;
% delta = nan(size(numbers));
% for num = 1:length(numbers)
%     position = borders(1) - stepwidth*numbers(num);
%     delta(num) = (zeropoint - position);
% end