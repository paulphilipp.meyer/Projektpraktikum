function sig = getsigmad(code)
if isequal(code(3), 'B')
    sig = 0.1;
elseif isequal(code(3), 'S')
    sig = 0.5;
else
    error('No sigmad for %s', code);
end