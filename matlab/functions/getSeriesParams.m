function params = getSeriesParams(code)
% GETSERIESPARAMS
%   params = getSeriesParams(code)
%   params(1): cell[int]            last Datafilenumber
%   params(2): cell[array(int)]     abscent Datafilenumbers
%   params(3): cell[string]         file Prefix
%   params(4): cell[array(int)]     usable for normalization
%   params(5): cell[array(double)]  start, end, zero
%   params(6): cell[double]         probendicke [mm]
%   available Series:
%   '3-0', '3-SF6_hi', '4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'

params = cell(6, 1);

if isequal(code, '1-0')
    params(1) = {27};
    params(2) = {[]};
    params(3) = {'data/First Try/20160603_Spektrum_without_SF6__USB2H003391_'};
elseif isequal(code, '3-0')
    params(1) = {45};
    params(2) = {6};
    params(3) = {'data/Third Try/20160603_Spektrum_without_SF6_3rd_try__USB2H003391_'};
    params(4) = {[1, -3:-1]};
    params(5) = {[13.66, 12.20, 12.44]};
elseif isequal(code, '3-SF6')
    params(1) = {45};
    params(2) = {[]};
    params(3) = {'data/Third Try with SF6/20160603_Spektrum_with_SF6_3rd_try__USB2H003391_'};
    params(5) = {[9.34, 8.90, 9.10]};
elseif isequal(code, '3-SF6_hi')
    params(1) = {44};
    params(2) = {[]};
    params(3) = {'data/Third Try with SF6 higher Intensity/20160603_Spektrum_with_SF6_3rd_try_higher_Intensity_USB2H003391_'};
    params(5) = {[9.34, 8.90, 9.10]};
elseif isequal(code, '4-0')
    params(1) = {62};
    params(2) = {[]};
    params(3) = {'data/Fourth Try/20160607_Spektrum_without_SF6_4th_try_USB2H003391_'};
    params(4) = {[1:12,-3:-1]};
    params(5) = {[12.80, 12.20, 12.46]};
elseif isequal(code, '4-BK7')
    params(1) = {55};
    params(2) = {[]};
    params(3) = {'data/Fourth Try with BK7/20160607_Spektrum_with_BK7_4th_try_USB2H003391_'};
    params(5) = {[12.12, 11.60, 11.84]};
elseif isequal(code, '4-SF6')
    params(1) = {62};
    params(2) = {[]};
    params(3) = {'data/Fourth Try with SF6/20160607_Spektrum_with_SF6_4th_try_USB2H003391_'};
    params(4) = {[1:6, -8:-20]};
    params(5) = {[8.51, 8.00, 8.30]};
elseif isequal(code, '6-0')
    params(1) = {51};
    params(2) = {[]};
    params(3) = {'data/Sixth Try/20160610_Spektrum_without_6th_try_USB2H003391_'};
    params(4) = {[1:4,-2:-1]};
    params(5) = {[14.56, 14.06, 14.30]};
elseif isequal(code, '6-BK7')
    params(1) = {45};
    params(2) = {[]};
    params(3) = {'data/Sixth Try with BK7/20160610_Spektrum_with_BK7_6th_try_USB2H003391_'};
    params(5) = {[10.64, 10.20, 10.45]};
elseif isequal(code, '6-SF6')
    params(1) = {51};
    params(2) = {[]};
    params(3) = {'data/Sixth Try with SF6/20160610_Spektrum_with_SF6_6th_try_USB2H003391_'};
    params(5) = {[8.42, 7.92, 8.16]};
else
    error('Data for code ''%s'' not found in function getSeriesParams', code);
end

if isequal(code(3), '0')
    params(6) = {0};
elseif isequal(code(3), 'S')
    params(6) = {5}; % \pm 0.5
elseif isequal(code(3), 'B')
    params(6) = {1.08}; % \pm 0.1
else
    error('komischer code %s', code)
end
