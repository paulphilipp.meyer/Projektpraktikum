function intens = readSeriesSimple(code)
% READSERIESSIMPLE
%   intens = readSeriesSimple(code)
    
    params = getSeriesParams(code);
    maxFileNum = cell2mat(params(1));
    missingFileNums = cell2mat(params(2));
    filePrefix = cell2mat(params(3));
    % number of existing measurements
    numMeas =  maxFileNum - size(missingFileNums, 2);
    
    intens = zeros(numMeas, size(getLambs(),2));
    skipped = 0;
    for i=1:maxFileNum
        % skip missing files
        if ismember(i, missingFileNums)
            skipped = skipped +1;
            continue
        end
        %disp(strcat(filePrefix, num2str(i, '%02i'), '.txt'))
        intens(i-skipped, :) = textread(strcat(filePrefix, num2str(i, '%02i'), '.txt'), '%*f\t%f');
    end