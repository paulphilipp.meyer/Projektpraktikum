function filename = codeToFilename(code, number)
% CODETOFILENAME 
%  filename = codeToFilename(code, number)
[lastDat, abscentDats, filePref] = measureSeriesFileParams(code);
if number < 0
    number = lastDat + number +1;
end
if number > lastDat
    error(['Measurement ', code, ' has fewer than ', num2str(number), ' measurements'])
end
if ismember(number, abscentDats)
    error(['Measurement ', code, ' has no measurement number ', num2str(number)])
end
filename = strcat(filePref, num2str(number, '%02i'), '.txt');
