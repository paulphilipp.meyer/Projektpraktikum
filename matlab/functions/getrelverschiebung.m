function verschiebung = getrelverschiebung(code, numbers)

load('constants', 'stepwidth');

if isequal(code, '4-0')
    numzero = 37;
elseif isequal(code, '4-BK7')
    numzero = 28;
elseif isequal(code, '4-SF6')
    numzero = 32;
elseif isequal(code, '6-0')
    numzero = 27;
elseif isequal(code, '6-BK7')
    numzero = 21;
elseif isequal(code, '6-SF6')
    numzero = 28;
else
    error('getrelverschiebung von %s nicht unterstützt', code)
end

verschiebung = -stepwidth*(numbers - numzero);