function i = indexFromValue(data, value, descending)
% INDEXFROMVALUE
%   i = indexFromValue(data, value, descending)
%   without descending:
%       returns index of first entry of data wich is >= value
%       if none is: i = -1
%   if descending defined:
%       returns index of last entry of data wich is >= value
%       if none is: i = -1
if ~exist('descending', 'var')
    for i=1:size(data, 2)
        if data(1, i) >= value
            return
        end
    end
    i = i+1;
else
    for i=size(data, 2)+1-(1:size(data, 2))
        if data(1, i) >= value
            return
        end
    end
    i = 0;
end
    