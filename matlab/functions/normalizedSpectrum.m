function normalized = normalizedSpectrum(code, number)
% NORMALIZEDSPECTRUM
%   intens = normalizedSpectrum(code, (number))
%   if no number given returns normalized series
if exist('number', 'var')
    normalized = readMeasurement(code, number)./spectralDensity(code);
    return
else
    intens = readSeriesSimple(code);
    normalized = intens./repmat(spectralDensity(code), size(intens, 1), 1);
end