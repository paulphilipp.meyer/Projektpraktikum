function mparams = getMeasParams(code, number)
% GETMEASPARAMS
%   mparams = getMeasParams(code, number)
%   nparams is cell
%   nparams(1) = {[lfc, rfc]}
%       lfc: linke grenze des rechten peaks im fourierraum
%            -> left fourier cut
%       rfc: rechte grenze des rechten peaks im fourierraum
%            -> right fourier cut


if ~ismember(code, getAvailableSeries())
    error('%s is not available yet', code)
end
if ~ismember(number, getMeas(code))
    error('no file number %i in serie %s', number, code)
end

mparams = cell(1, 1);
if isequal(code, '4-0')
    if number == 20
        mparams(1) = {[1395, 1515]};
    elseif number == 30
        mparams(1) = {[1175, 1241]};
    elseif number == 40
        mparams(1) = {[1082, 1145]};
    elseif number == 30
        mparams(1) = {[1302, 1433]};
    else
        warning('getMeasParams enthält keine Informationen für %s, %i', code, number)
    end
elseif isequal(code, '4-BK7')
    if number == 10
        mparams(1) = {[1414, 1526]};
    elseif number == 20
        mparams(1) = {[1196, 1296]};
    elseif number == 30
        mparams(1) = {[1063, 1105]};
    elseif number == 40
        mparams(1) = {[1282, 1396]};
    else
        warning('getMeasParams enthält keine Informationen für %s, %i', code, number)
    end
elseif isequal(code, '4-SF6')
    if number == 36
        mparams(1) = {[1069, 1262]};
    elseif number == 37
        mparams(1) = {[1052, 1282]};
    elseif number == 38
        mparams(1) = {[1095, 1283]};
    elseif number == 39
        mparams(1) = {[1127, 1300]};
    elseif number == 39
        mparams(1) = {[1095, 1283]};
    elseif number == 40
        mparams(1) = {[1129, 1315]};
    elseif number == 41
        mparams(1) = {[1175, 1329]};
    elseif number == 42
        mparams(1) = {[1179, 1380]};
    elseif number == 43
        mparams(1) = {[1193, 1412]};
    elseif number == 44
        mparams(1) = {[1219, 1412]};
    elseif number == 45
        mparams(1) = {[1274, 1438]};
    elseif number == 46
        mparams(1) = {[1297, 1450]};
    elseif number == 47
        mparams(1) = {[1317, 1487]};
    elseif number == 48
        mparams(1) = {[1369, 1506]};
    elseif number == 49
        mparams(1) = {[1391, 1529]};
    elseif number == 50
        mparams(1) = {[1430, 1551]};
    elseif number == 51
        mparams(1) = {[1465, 1558]};
    elseif number == 52
        mparams(1) = {[1497, 1597]};    
    else
        warning('getMeasParams enthält keine Informationen für %s, %i', code, number)
    end  
elseif isequal(code, '6-0')
    if number == 10
        mparams(1) = {[1384, 1513]}; 
    elseif number == 20
        mparams(1) = {[1176, 1231]};
    elseif number == 30
        mparams(1) = {[1083, 1139]};
    elseif number == 30
        mparams(1) = {[1304, 1429]};
    else
        warning('getMeasParams enthält keine Informationen für %s, %i', code, number)
    end
elseif isequal(code, '6-BK7')
    if number == 10
        mparams(1) = {[1262, 1359]};
    elseif number == 20
        mparams(1) = {[1107, 1148]};
    elseif number == 30
        mparams(1) = {[1212, 1268]};
    else
        warning('getMeasParams enthält keine Informationen für %s, %i', code, number)
    end
elseif isequal(code, '6-SF6')
    if number == 10
        mparams(1) = {[1303, 1631]};
    elseif number == 20
        mparams(1) = {[1132, 1380]};
    elseif number == 30
        mparams(1) = {[1058, 1186]};
    elseif number == 40
        mparams(1) = {[1233, 1420]};
    elseif number == 45
        mparams(1) = {[1384, 1522]};
    else
        warning('getMeasParams enthält keine Informationen für %s, %i', code, number)
    end  
else
    error('getMeasParams enthält keine Informationen für %s, %i', code, number)
end


        