function verschiebung = getnullverschiebung(code)
% VERSCHIEBUNG
% gibt die gemessene verschiebung des Nullpunktes relativ zur zugehörigen
% messung ohne Probe zurück

load('constants', 'stepwidth');
params = getSeriesParams(code);
borders = cell2mat(params(5));
zeroseries = borders(3);

paramsnull = getSeriesParams([code(1), '-0']);
bordersnull = cell2mat(paramsnull(5));
zeropoint = bordersnull(3);  
    
verschiebung = zeroseries - zeropoint;