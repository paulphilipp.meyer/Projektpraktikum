function measurements = getMeas(code)
% GETMEAS
%   measurements = getMeas()
%   measurements: cell code x array(dateinnummern)

params = getSeriesParams(code);
if ~isequal(params(2), {[]})
        error('getMeas hat Serie %s mit fehlenden Dateien: %s erhalten und kann das noch nicht verarbeiten', code, mat2str(cell2mat(params(2))))
end
measurements = 1:cell2mat(params(1));

   %{
series = getAvailableSeries();
measurements = cell(length(series), 2);
measurements(:,1) = series;
for count = 1:length(series)
    s = cell2mat(series(count));
    params = getSeriesParams(s);
    if ~isequal(params(2), {[]})
        error('getMeas hat Serie mit fehlenden Dateien: %s erhalten und kann das noch nicht verarbeiten', mat2str(cell2mat(params(2))))
    end
    filenumbers = 1:cell2mat(params(1));
    measurements(count, 2) = {filenumbers};
end
    %}