function cprintInterspektrogramm(code, meas, lambs, intens, minLamb, maxLamb, minn, maxn)
%CPRINTINTERSPEKTROGRAMM 
%   cprintInterspektrogramm(code meas, lambs, intens, minLamb, maxLamb, minn, maxn)
%   available Series:
%   '3-0', '3-SF6_hi', '4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'
    
    if ~exist('minLamb', 'var')
        minLamb = 1;
    else
        minLamb = indexFromValue(lambs, minLamb)-1;
    end
    if ~exist('maxLamb', 'var')
        maxLamb = size(lambs, 2);
    else
        maxLamb = indexFromValue(lambs, maxLamb)-1;
    end
    if ~exist('minn', 'var')
        minn = 1;
    end
    if ~exist('maxn', 'var')
        maxn = size(meas, 2);
    end
    imagesc(lambs(1,minLamb:maxLamb), meas(1,minn:maxn), intens(minn:maxn,minLamb:maxLamb));
    xlabel('Wellenlänge \lambda [nm]');
    ylabel('Nummer der Messung');

    print(strcat('images/', code, '.png'), '-dpng');

