function series = getAvailableSeries()
%series = {'3-0', '3-SF6_hi', '4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'};
series = {'4-0', '4-BK7', '4-SF6', '6-0', '6-BK7', '6-SF6'};