function [meas, lambs, intens] = readSeries(numFiles, missingFiles, filePrefix)
% READSERIES
%   [meas, lambs, intens] = READSERIES (numFiles, missingFiles, filePrefix)
    numMeas = numFiles-size(missingFiles,1);
    [lambs, ~] = textread(strcat(filePrefix, '01.txt'), '%f\t%f');
    lambs = lambs.';
    %normGrid = repmat(norm, 1, numMeas);
    skipped = 0;
    meas = 1:numFiles-size(missingFiles, 2);
    intens = zeros(numMeas, size(lambs,2));
    for i=1:numFiles
        if ismember(i, missingFiles)
            skipped = skipped + 1;
            continue
        end
        %meas = [meas, i];
        disp(strcat(filePrefix, num2str(i, '%02i'), '.txt'))
        intens(i-skipped, :) = textread(strcat(filePrefix, num2str(i, '%02i'), '.txt'), '%*f\t%f');
    end
end

%numFiles = 45;
%missingFiles = [6];
%filePrefix = 'data/Third Try/20160603_Spektrum_without_SF6_3rd_try__USB2H003391_';
%readSeries(numFiles, missingFiles, filePrefix);

%[firstlamb, firstIntens] = textread(strcat(filePrefix, '01.txt'), '%f\t%f');
%plot(firstFile)
%grid on

%minLamb=1150;
%maxLamb=1700;

%pcolor(meas, lambs(minLamb:maxLamb), intens(minLamb:maxLamb, :))
%surf(meas, lambs(minLamb:maxLamb), intens(minLamb:maxLamb, :))