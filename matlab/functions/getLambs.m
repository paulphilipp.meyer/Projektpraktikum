function lambs = getLambs(code, number)
% GETLAMBS 
%   lambs = getLambs()
% returns vector containing measured wavelength values
%   default: measurement '3-0' number 1

if ~exist('code', 'var')
    code = '3-0';
    number = 1;
elseif ~exist('number', 'var')
    number = 1;
end
filename = codeToFilename(code, number);
lambs = textread(filename, '%f\t%*f').';
%[lambs, ~] = readMeasurement(code, number);