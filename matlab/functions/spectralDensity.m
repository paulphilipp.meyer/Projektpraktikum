function intens = spectralDensity(code)
% SPECTRALDENSITY
%   intens = spectralDensity(code)
% returns vector containing the spectral density of the laser without
% interference
    params = getSeriesParams(code);
    usableMeas = cell2mat(params(4));
    measurements = [];
% einlesen
    for number = usableMeas
        measurements = [measurements; readMeasurement(code, number)]; %#ok<*AGROW>
    end
% mitteln
    intens = mean(measurements, 1);