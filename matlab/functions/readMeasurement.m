function intens = readMeasurement(code, number)
% READMEASUREMENT
%   [lambs, intens] = readMeasurement('code', number)
filename = codeToFilename(code, number);
intens = textread(filename, '%*f\t%f').';