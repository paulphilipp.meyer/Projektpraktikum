function kappa = getkappa(code, numbers)

kappa = getnullverschiebung(code) + getrelverschiebung(code, numbers);